# Stack machine example

This is an example 16-bit stack machine for teaching computer architecture.

The machine has a 16-bit integer data type, 4K each of code and data memory,
a value stack (capacity 256), and a return stack (capacity 256).

Instructions are 16 bits long; instructions that take an argument are a total
of 32 bits long.

See the documentation (not currently in the repo) for details of the instructions.